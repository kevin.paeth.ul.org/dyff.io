## Dev Server

Install dependencies with `npm install`

Start dev server with `make all`

## Adding blog posts

Create a new markdown (.md) file in the `blog` directory with a fitting filename. Markdown files in `blog/` are exposed as routes as `dyff.io/blogs/<file_name>`.

Add the following at the top of this file:

```
---
title: Some blog title
date: YYYY-MM-DD
authors: [yourname, othername]
tags: [some, tags]
description: Description of article
---
```

The values in `tags: []` will be added as meta tags in the HTML head.

The authors in the `authors: []` directive reference values in `_data/profiles.json`. Add an entry for each author in that file:

```json
{
  "yourname": {
    "name": "Your Name Jr."
  },
  "othername": {
    "name": "Other Name Sr."
  }
}
```

## Static Assets

- Static assets are stored in the static bucket `assets.dyff.io`

Install [minio client](<(https://min.io/docs/minio/linux/reference/minio-mc.html)>) - see [dev-environment minio setup](https://gitlab.com/dyff/dev-environment#access-minio)

Assuming procurement of Bitwarden access to `Cloudflare R2 dyff-io-assets bucket API token`, utilize the following credentials when adding an alias:

```bash
mc alias set cloudflare <Endpoint> <Access Key ID> <Secret Access Key>
```

You can now copy files into the bucket using

```bash
mc cp <local file> cloudflare/dyff-io-assets/<subpath>
```

List files using

```bash
mc ls cloudflare/dyff-io-assets
```
